export class PriceList {
  priceListId: number;
  priceListName: string;
  extErpPriceListID: number | null;
}
