import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ErpLogisticSiteServiceService {
  constructor() {}

  getErps() {
    let data = JSON.parse(localStorage.getItem('db') || '');
    return of(
      data.map((o: any) => {
        return o.extErpPriceListID;
      })
    );
  }
}
