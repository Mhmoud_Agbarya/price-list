import { TestBed } from '@angular/core/testing';

import { ErpLogisticSiteServiceService } from './erp-logistic-site-service.service';

describe('ErpLogisticSiteServiceService', () => {
  let service: ErpLogisticSiteServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErpLogisticSiteServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
