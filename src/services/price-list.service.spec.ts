import { TestBed } from '@angular/core/testing';

import { PriceListService } from './price-list.service';

describe('PriceListService', () => {
  let service: PriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getPriceLists should return value from observable', (done: DoneFn) => {
    service.getPriceLists([1, 2, 3, 4], '').subscribe((value) => {
      expect(value).toBeTruthy();
      done();
    });
  });

  it('#updatePriceList should return value from observable', (done: DoneFn) => {
    service
      .updatePriceList(10000, 'ababababab', 10111, [1, 2, 3, 4])
      .subscribe((value) => {
        expect(value).toBeTruthy();
        done();
      });
  });

  it('#updatePriceList should return an error when input is not valid', (done: DoneFn) => {
    expect(
      service
        .updatePriceList(10000, 'ababababab', 10000, [1, 2, 3, 4])
        .subscribe((result) => {
          result;
        })
    ).toThrow(new Error('error'));
    done();
  });
});
