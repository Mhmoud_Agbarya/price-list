import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { PriceList } from 'src/models/price-list';
import DB from 'src/assets/db.json';

@Injectable({
  providedIn: 'root',
})
export class PriceListService {
  db = DB;
  tempPriceList = new PriceList();

  constructor() {}

  getPriceLists(
    companyIds: number[],
    searchFilter: string = ''
  ): Observable<Array<PriceList>> {
    let data = JSON.parse(localStorage.getItem('db') || '');
    if (searchFilter !== '') {
      let temp = data.filter((o: any) => {
        //search predicate can be modified to search only the names
        //it searches all fields just in case
        return (
          (o.extErpPriceListID.toString() == searchFilter ||
            o.priceListId.toString() == searchFilter ||
            o.priceListName.includes(searchFilter)) &&
          companyIds.includes(o.extErpPriceListID)
        );
      });
      return of(temp);
    } else {
      return of(data);
    }
  }

  updatePriceList(
    priceListID: number,
    newPriceListName: string,
    extErpPriceListID: number | null,
    companyIds: number[]
  ): Observable<Array<PriceList>> {
    try {
      let res = this.getPriceLists(companyIds, '');
      let flag = -1;
      //check for problematic index
      res.subscribe((o) => {
        o.forEach((e) => {
          if (e.priceListId == extErpPriceListID) {
            flag = 1;
          }
        });
      });
      if (flag != -1) {
        throw new Error('Invalid Input');
      }
      //save to a temporary object in case there are no problems
      this.tempPriceList = {
        priceListId: priceListID,
        priceListName: newPriceListName,
        extErpPriceListID: extErpPriceListID,
      };
      this.saveItem(this.tempPriceList);
      res = this.getPriceLists([], '');
      return res;
    } catch (e) {
      return throwError(e);
    }
  }

  saveItem(item: PriceList) {
    let tempDb = JSON.parse(localStorage.getItem('db') || '');
    let index = tempDb.findIndex((o: any) => o.priceListId == item.priceListId);
    tempDb[index] = item;
    localStorage.setItem('db', JSON.stringify(tempDb));
  }

  //Function used only once to save mock DB
  saveAll() {
    localStorage.setItem('db', JSON.stringify(this.db));
  }
}
