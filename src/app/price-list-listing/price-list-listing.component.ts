import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PriceList } from 'src/models/price-list';
import { ErpLogisticSiteServiceService } from 'src/services/erp-logistic-site-service.service';
import { PriceListService } from 'src/services/price-list.service';
import { PriceListEditComponent } from '../price-list-edit/price-list-edit.component';

@Component({
  selector: 'app-price-list-listing',
  templateUrl: './price-list-listing.component.html',
  styleUrls: ['./price-list-listing.component.scss'],
})
export class PriceListListingComponent implements OnInit {
  priceList = new Array<PriceList>();
  searchFormCtrl = new FormControl();
  erpList = [];

  constructor(
    private priceListService: PriceListService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private erpService: ErpLogisticSiteServiceService
  ) {}

  ngOnInit(): void {
    this.onGetErp();
    this.onGetList();
  }

  openDialog(item: PriceList): void {
    const dialogRef = this.dialog.open(PriceListEditComponent, {
      width: '250px',
      data: { item: item },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.priceListService
          .updatePriceList(
            result.priceListId,
            result.priceListName,
            result.extErpPriceListID,
            this.erpList
          )
          .subscribe(
            (res) => {
              this.priceList = res;
              this.onGetErp();
            },
            (err) => {
              this.snackBar.open(err);
              setTimeout(() => this.snackBar.dismiss(), 3000);
            }
          );
      }
    });
  }

  onGetList() {
    //get the values that fulfill the coditions set
    this.priceListService.getPriceLists(this.erpList).subscribe((list) => {
      this.priceList = list.map((o) => {
        return {
          priceListId: o.priceListId,
          priceListName: o.priceListName,
          extErpPriceListID: o.extErpPriceListID,
        } as PriceList;
      });
    });
    //reset the search filter
    this.searchFormCtrl.setValue('');
  }

  onGetErp() {
    this.erpService.getErps().subscribe((data) => (this.erpList = data));
  }

  onSearchSubmitted() {
    this.priceListService
      .getPriceLists(this.erpList, this.searchFormCtrl.value)
      .subscribe((result) => {
        this.priceList = result.map((o) => {
          return {
            priceListId: o.priceListId,
            priceListName: o.priceListName,
            extErpPriceListID: o.extErpPriceListID,
          } as PriceList;
        });
      });
  }
}
