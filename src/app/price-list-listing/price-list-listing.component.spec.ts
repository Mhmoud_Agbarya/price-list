import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';

import { PriceListListingComponent } from './price-list-listing.component';

describe('PriceListListingComponent', () => {
  let component: PriceListListingComponent;
  let fixture: ComponentFixture<PriceListListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PriceListListingComponent],
      providers: [MatDialog],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceListListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
