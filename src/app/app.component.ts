import { Component } from '@angular/core';
import { PriceListService } from 'src/services/price-list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Price_list';

  constructor(private priceListService: PriceListService) {
    this.priceListService.saveAll();
  }
}
