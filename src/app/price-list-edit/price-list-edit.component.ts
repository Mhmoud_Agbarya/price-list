import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PriceList } from 'src/models/price-list';
import { PriceListService } from 'src/services/price-list.service';

@Component({
  selector: 'app-price-list-edit',
  templateUrl: './price-list-edit.component.html',
  styleUrls: ['./price-list-edit.component.scss'],
})
export class PriceListEditComponent implements OnInit {
  newItem = new PriceList();
  priceListForm = new FormGroup({});
  newPriceListNameCtrl = new FormControl();
  extErpPriceListIDCtrl = new FormControl();

  constructor(
    private priceListService: PriceListService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<PriceListEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { item: PriceList }
  ) {}

  ngOnInit(): void {
    this.priceListForm = this.fb.group({
      newPriceListNameCtrl: new FormControl(),
      extErpPriceListIDCtrl: new FormControl(),
    });
    this.newPriceListNameCtrl.setValue(this.data.item.priceListName);
    this.extErpPriceListIDCtrl.setValue(this.data.item.extErpPriceListID);
    this.newPriceListNameCtrl.setValidators([
      Validators.minLength(10),
      Validators.required,
    ]);
    this.extErpPriceListIDCtrl.setValidators([Validators.required]);
  }

  onUpdate() {
    this.newItem.extErpPriceListID = this.extErpPriceListIDCtrl.value;
    this.newItem.priceListName = this.newPriceListNameCtrl.value;
    this.newItem.priceListId = this.data.item.priceListId;
    if (this.newPriceListNameCtrl.valid) {
      this.dialogRef.close(this.newItem);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
