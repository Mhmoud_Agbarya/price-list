import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { PriceListEditComponent } from './price-list-edit.component';

describe('PriceListEditComponent', () => {
  let component: PriceListEditComponent;
  let fixture: ComponentFixture<PriceListEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PriceListEditComponent],
      providers: [FormsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
