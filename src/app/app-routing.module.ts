import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PriceListListingComponent } from './price-list-listing/price-list-listing.component';

const routes: Routes = [{ path: '', component: PriceListListingComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
